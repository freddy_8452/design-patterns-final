package edu.alenasoft.sesion9;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class Luigis {

    public static void main(String[] args) {
        Food pizza = new Pizza();
        System.out.println(pizza.describe() + " " + pizza.getPrice());

        pizza = new Jamon(pizza);
        System.out.println(pizza.describe() + " " + pizza.getPrice());

        pizza = new Tomatoe(pizza);
        System.out.println(pizza.describe() + " " + pizza.getPrice());

        pizza = new Ingredient("Cucumber", 3, pizza);
        System.out.println(pizza.describe() + " " + pizza.getPrice());

        pizza = new Ingredient("Borde", 7, pizza);
        System.out.println(pizza.describe() + " " + pizza.getPrice());
    }
}
