package edu.alenasoft.sesion9;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class Ingredient implements Food {

    private String name;
    private Food food;
    private int price;

    public Ingredient(String name, int price, Food food) {
        this.name = name;
        this.food = food;
        this.price = price;
    }

    public String describe() {
        return String.format("%s + %s", food.describe(), name);
    }

    public int getPrice() {
        return food.getPrice() + price;
    }
}
