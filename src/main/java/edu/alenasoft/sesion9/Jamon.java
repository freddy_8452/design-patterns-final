package edu.alenasoft.sesion9;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class Jamon implements Food {

    private Food food;

    public Jamon(Food food) {
        this.food = food;
    }

    public String describe() {
        // ConcreteFoodName + Jamon
        return String.format("%s %s", food.describe(), "+ Jamon");
    }

    public int getPrice() {
        return food.getPrice() + 15;
    }
}
