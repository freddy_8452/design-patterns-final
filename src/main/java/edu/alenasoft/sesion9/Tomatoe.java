package edu.alenasoft.sesion9;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class Tomatoe implements Food {

    private Food food;

    public Tomatoe(Food food) {
        this.food = food;
    }

    public String describe() {
        return String.format("%s %s", food.describe(), " + Tomatoe");
    }

    public int getPrice() {
        return food.getPrice() + 5;
    }
}
