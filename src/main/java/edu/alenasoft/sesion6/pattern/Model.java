package edu.alenasoft.sesion6.pattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class Model {

    private int averageAges;
    private List<Observer> observers;

    public Model() {
        this.averageAges = 15;
        this.observers = new ArrayList<Observer>();
    }

    public void attach(Observer observer) {
        this.observers.add(observer);
    }

    public void detach(Observer observer) {
        this.observers.remove(observer);
    }

    private void notifyToObserver() {
        for (Observer o : this.observers) {
            o.doAction(this.averageAges);
        }
    }

    public void setAverageAge(int averageAge) {
        this.averageAges = averageAge;
        this.notifyToObserver();
    }
}
