package edu.alenasoft.sesion6.pattern;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public interface Observer {

    void doAction(int age);
}
