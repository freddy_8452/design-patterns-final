package edu.alenasoft.sesion6.classic;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class Model {

    private int averageAges;

    public Model() {
        this.averageAges = 0;
    }

    public void increaseAverage() {
        this.averageAges++;
    }

    public int getAverage() {
        return this.averageAges;
    }

}
