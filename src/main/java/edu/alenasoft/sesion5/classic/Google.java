package edu.alenasoft.sesion5.classic;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class Google {

    private String[] users;

    public Google() {
        this.users = new String[3];
        // Fill data from DB
        this.users[0] = "G. Samuel";
        this.users[1] = "G. Tuto";
        this.users[2] = "G. Evo";
    }

    public String[] getGooglers() {
        return users;
    }
}
