package edu.alenasoft.sesion5.classic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class Facebook {

    private ArrayList<String> users;

    public Facebook() {
        this.users = new ArrayList<String>();
        // DB is filled
        this.users.add("F. Gerardo");
        this.users.add("F. Miguel");
        this.users.add("F. Alvaro");
    }

    public ArrayList getUsers() {
        return users;
    }
}
