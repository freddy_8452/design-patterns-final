package edu.alenasoft.sesion5.pattern;

import edu.alenasoft.sesion5.pattern.behaviors.Aggregate;
import edu.alenasoft.sesion5.pattern.behaviors.Iterator;
import edu.alenasoft.sesion5.pattern.implementations.aggregates.Cre;
import edu.alenasoft.sesion5.pattern.implementations.aggregates.Facebook;
import edu.alenasoft.sesion5.pattern.implementations.aggregates.Google;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class Main {

    public static void main(String[] args) {
        List<Aggregate> companies = new ArrayList<Aggregate>();
        companies.add(new Google());
        companies.add(new Facebook());
        companies.add(new Cre());

        for (Aggregate company : companies) {
            printElements(company.createIterator());
        }
    }

    private static void printElements(Iterator iterator) {
        while (!iterator.isDone()) {
            System.out.println(iterator.next());
        }
    }
}
