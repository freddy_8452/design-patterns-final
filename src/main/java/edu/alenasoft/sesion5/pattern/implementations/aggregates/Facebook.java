package edu.alenasoft.sesion5.pattern.implementations.aggregates;

import edu.alenasoft.sesion5.pattern.implementations.iterators.FacebookUsersIterator;
import edu.alenasoft.sesion5.pattern.behaviors.Aggregate;
import edu.alenasoft.sesion5.pattern.behaviors.Iterator;

import java.util.ArrayList;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class Facebook implements Aggregate {

    private ArrayList<String> users;

    public Facebook() {
        this.users = new ArrayList<String>();
        // DB is filled
        this.users.add("F. Gerardo");
        this.users.add("F. Miguel");
        this.users.add("F. Alvaro");
    }

    public ArrayList getUsers() {
        return users;
    }

    public Iterator createIterator() {
        return new FacebookUsersIterator(this);
    }
}
