package edu.alenasoft.sesion5.pattern.implementations.iterators;

import edu.alenasoft.sesion5.pattern.behaviors.Iterator;
import edu.alenasoft.sesion5.pattern.implementations.aggregates.Cre;

/**
 * Created by Luis Roberto Perez on 19/10/2016.
 */
public class CreIterator implements Iterator<String> {
    private int position;
    private Cre cre;

    public CreIterator(Cre cre) {
        this.cre = cre;
    }

    public String current() {
        return Integer.toString(this.cre.getIds()[this.position]);
    }

    public String next() {
        return Integer.toString(this.cre.getIds()[this.position++]);
    }

    public boolean isDone() {
        return this.position >= this.cre.getIds().length;
    }
}
