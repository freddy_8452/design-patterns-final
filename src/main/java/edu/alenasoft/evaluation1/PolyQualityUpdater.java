package edu.alenasoft.evaluation1;

/**
 * Created by Luis Roberto Perez on 31/10/2016.
 */
public class PolyQualityUpdater implements Updater {

    public void update(Item item) {
        if(item.getSellIn()>10){
            item.setQuality(item.getQuality()+1);
        }else if(item.getSellIn()>5){
            item.setQuality(item.getQuality()+2);
        }else {
            item.setQuality(item.getQuality()+3);
        }
        if(item.getSellIn()<=0){
            item.setQuality(0);
        }

    }
}
