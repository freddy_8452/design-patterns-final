package edu.alenasoft.evaluation1;

/**
 * Created by Luis Roberto Perez on 31/10/2016.
 */
public class IncreaseQualityUpdater implements Updater {

    private int delta;

    public IncreaseQualityUpdater() {
        this.delta = 1;
    }

    public IncreaseQualityUpdater(int delta) {
        this.delta = delta;
    }

    public void update(Item item) {
        item.setQuality(item.getQuality() + delta);
    }
}
