package edu.alenasoft.evaluation1;

import java.util.ArrayList;
import java.util.List;


public class GildedRose {

    public static List<Item> items = null;

    /**
     * @param args
     */
    public static void main(String[] args) {

        System.out.println("OMGHAI!");

        items = new ArrayList<Item>();
        items.add(new Item("+5 Dexterity Vest", 10, 20));
        items.add(new Item("Aged Brie", 2, 0));
        items.add(new Item("Elixir of the Mongoose", 5, 7));
        items.add(new Item("Sulfuras, Hand of Ragnaros", 0, 80));
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20));
        items.add(new Item("Conjured Mana Cake", 3, 6));

        updateQuality();
    }


    public static void updateQuality() {
        for (Item item:items) {
            if(item.getQuality() >= 50) continue;

            Updater updater = getUpdater(item.getName());
            updater.update(item);

            if(item.getQuality() <= 0) item.setQuality(0);
        }
    }

    private static Updater getUpdater(String name) {
        if (name.contains("Aged Brie"))
            return new IncreaseQualityUpdater();
        if (name.contains("Sulfuras"))
            return new KeepQualityUpdater();
        if (name.contains("Backstage passes"))
            return new PolyQualityUpdater();
        if (name.contains("Conjured"))
            return new IncreaseQualityUpdater(2);

        return new DecreaseQualityUpdater();
    }

}