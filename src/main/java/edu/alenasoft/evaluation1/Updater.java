package edu.alenasoft.evaluation1;

/**
 * Created by Luis Roberto Perez on 31/10/2016.
 */
public interface Updater {
    void update(Item item);
}
