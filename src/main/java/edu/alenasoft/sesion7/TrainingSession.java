package edu.alenasoft.sesion7;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class TrainingSession {

    public static void main(String[] args) {
        Team blooming = new Team(1500);

        PolicyStrategy workRule = new PolicyStrategy() {
            public void doWork(int money) {
                if(money < 2000) {
                    System.out.println("Huelga");
                } else {
                    System.out.println("Train");
                }
            }
        };

        blooming.setPolicyStrategy(workRule);

        Team bolivar = new Team(15000);
        bolivar.setPolicyStrategy(workRule);

        blooming.train();
        blooming.setMoney(8000);
        blooming.train();

        bolivar.train();
    }
}
