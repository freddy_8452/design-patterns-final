package edu.alenasoft.sesion7;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class Team {

    private int money;
    private Strategy strategy;
    private PolicyStrategy policyStrategy;

    public Team(int money) {
        this.money = money;
    }

    public void play() {
        this.strategy.doWork();
    }

    public void train() {
        this.policyStrategy.doWork(this.money);
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public void setPolicyStrategy(PolicyStrategy policyStrategy) {
        this.policyStrategy = policyStrategy;
    }

    public void setMoney(int money){
        this.money = money;
    }
}
