package edu.alenasoft.sesion7;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public class AttackStrategy implements Strategy {
    public void doWork() {
        System.out.println("Attack");
    }
}
