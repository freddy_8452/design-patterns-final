package edu.alenasoft.sesion7;

/**
 * Created by Luis Roberto Perez on 24/10/2016.
 */
public interface PolicyStrategy {

    void doWork(int money);
}
