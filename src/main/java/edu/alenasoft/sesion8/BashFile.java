package edu.alenasoft.sesion8;

/**
 * Created by Luis Roberto Perez on 26/10/2016.
 */
public class BashFile implements File {

    private String fileName;

    public BashFile(String fileName) {
        this.fileName = fileName + ".sh";
    }

    public void printInfo() {
        System.out.println(this.fileName);
    }
}
