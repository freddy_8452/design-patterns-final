package edu.alenasoft.sesion8;

/**
 * Created by Luis Roberto Perez on 26/10/2016.
 */
public class WordFile implements File {

    private String fileName;

    public WordFile(String fileName){
        this.fileName = fileName + ".docx";
    }

    public void printInfo() {
        System.out.println(this.fileName);
    }
}
