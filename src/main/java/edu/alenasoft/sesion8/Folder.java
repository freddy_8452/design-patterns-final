package edu.alenasoft.sesion8;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 26/10/2016.
 */
public class Folder implements File {

    private List<File> elements;
    private String folderName;

    public Folder(String folderName) {
        this.elements = new ArrayList<File>();
        this.folderName = folderName;
    }

    public void addFile(File file) {
        this.elements.add(file);
    }

    public void printInfo() {
        System.out.println(this.folderName);
        for (File file : elements) {
            file.printInfo();
        }
    }
}
