package edu.alenasoft.sesion1;

import edu.alenasoft.sesion1.model.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 10/10/2016.
 */
public class Main {
    public static void main(String arg[]) {
        System.out.println("Hello Design Pattern World");

        String name = "Luis Roberto";
        int age = 25;
        double size = 1.93;
        boolean exists = false;

        if(4 == 5) {
            System.out.println("4 == 5");
        } else {
            System.out.println("4 == 5 is not true");
        }

        for (int i = 1; i < 5; i++) {
            System.out.println(i);
        }

        Person var = new Person("Daniel");
        Person var2 = var;

        Person var3 = new Person("Daniel");

        if (var.equals(var2)) {
            System.out.println("Equals var & var2");
        }

        if (var2.equals(var3)) {
            System.out.println("Equals var & var2");
        }

        int[] numbers = new int[5];
        int[] numbers2 = {5,3,8,87};

        List<Integer> dynamicList = new ArrayList<Integer>();

        System.exit(0);
    }
}
