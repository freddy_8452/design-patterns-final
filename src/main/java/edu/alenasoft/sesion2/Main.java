package edu.alenasoft.sesion2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Luis Roberto Perez on 10/10/2016.
 */
public class Main {
    public static void main(String[] arg) {
        //JOptionPane.showMessageDialog(null, "Hello");
        //JOptionPane.showMessageDialog(null, "CCAT",
                //"A message", JOptionPane.ERROR_MESSAGE);

        Icon icono = new BoxIcon(150, Color.blue);

        Icon iconSmallRed = new BoxIcon(50, Color.red);
        JOptionPane.showMessageDialog(null, "CCAT",
                "A message", JOptionPane.ERROR_MESSAGE, iconSmallRed);
    }
}
