package edu.alenasoft.sesion10;

/**
 * Created by Luis Roberto Perez on 7/11/2016.
 */
public class Main {

    public static void main(String[] args) {

        for(int i=0; i<1500; i++) {

            Thread thread = new Thread(new Runnable() {
                public void run() {
                    System.out.println(SimpleSingleton.simpleSingleton);
                    System.out.println(SimpleSingleton.simpleSingleton.data);
                }
            });

            Thread thread2 = new Thread(new Runnable() {
                public void run() {
                    System.out.println(SimpleSingleton.simpleSingleton);
                    System.out.println(SimpleSingleton.simpleSingleton.data);
                }
            });

            thread.start();
            thread2.start();

            System.out.println("--------------------");
        }

    }
}
