package edu.alenasoft.sesion4;

/**
 * Created by Luis Roberto Perez on 17/10/2016.
 */
public class FlyNoWay implements FlyBehavior {
    public void fly() {
        System.out.println("No volar");
    }
}
