package edu.alenasoft.sesion4;

/**
 * Created by Luis Roberto Perez on 17/10/2016.
 */
public class Main {
    public static void main(String[] args) {
        FlyBehavior wings = new FlyWithWings();
        FlyBehavior none = new FlyNoWay();

        Duck mallardDuck = new Duck(wings,new QuackStandard());
        Duck rubberDuck = new Duck(none,new QuackRubber());
        Duck rocketDuck = new Duck( new FlyRocket(),new QuackStandard() );

        mallardDuck.fly();
        mallardDuck.quack();
        rubberDuck.fly();
        rubberDuck.quack();
        rocketDuck.fly();

        //No es esperado
        rocketDuck.quack();
        rocketDuck.setQuackBehavior(new QuackRubber());
        rocketDuck.quack();
    }
}
