package edu.alenasoft.sesion4;

/**
 * Created by Luis Roberto Perez on 17/10/2016.
 */
public class QuackRubber implements QuackBehavior {
    public void quack() {
        System.out.println("squick squick");
    }
}
