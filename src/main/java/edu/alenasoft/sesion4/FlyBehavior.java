package edu.alenasoft.sesion4;

/**
 * Created by Luis Roberto Perez on 17/10/2016.
 */
public interface FlyBehavior {

    void fly();
}
