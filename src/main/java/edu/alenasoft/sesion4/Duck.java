package edu.alenasoft.sesion4;

/**
 * Created by Luis Roberto Perez on 17/10/2016.
 */
public class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public Duck(FlyBehavior flyBehavior, QuackBehavior quackBehavior) {
        this.flyBehavior = flyBehavior;
        this.quackBehavior = quackBehavior;
    }

    public void fly() {
        this.flyBehavior.fly();
    }

    public void quack(){
        this.quackBehavior.quack();
    }

    public void swin() {}
    public void display() {}

    public void setQuackBehavior(QuackBehavior quackBehavior) {
        this.quackBehavior = quackBehavior;
    }
}
