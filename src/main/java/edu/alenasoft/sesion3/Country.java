package edu.alenasoft.sesion3;

/**
 * Created by Luis Roberto Perez on 12/10/2016.
 */
public class Country implements Comparable<Country> {
    private String name;
    private long people;

    public Country(String name, long people) {
        this.name = name;
        this.people = people;
    }

    public int compareTo(Country country) {
        return this.name.compareTo(country.name);
    }

    @Override
    public String toString() {
        return String.format("%s %d hab.", this.name, this.people);
    }

    public long getPeople() {
        return this.people;
    }
}
