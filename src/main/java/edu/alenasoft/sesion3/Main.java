package edu.alenasoft.sesion3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Luis Roberto Perez on 12/10/2016.
 */
public class Main {
    public static void main(String[] args) {
        List countries = new ArrayList<String>();
        countries.add("Ecuador");
        countries.add("Brasil");
        countries.add("Bolivia");

        System.out.println(countries);
        Collections.sort(countries);
        System.out.println(countries);
    }
}
