package edu.alenasoft.sesion3;

import java.util.Comparator;

/**
 * Created by Luis Roberto Perez on 12/10/2016.
 */
public class ComparatorByPeople implements Comparator<Country> {
    public int compare(Country base, Country next) {
        if (base.getPeople() < next.getPeople()) return -1;
        if (base.getPeople() > next.getPeople()) return 1;
        return 0;
    }
}
