package edu.alenasoft.evaluation1;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Luis Roberto Perez on 26/10/2016.
 */
public class GildedRoseTest {

    private GildedRose gildedRose;

    @Before
    public void setUp() {
        gildedRose.items = new ArrayList<Item>();
    }

    /**
     *  El Quality de un item nunca es un número negativo.
     */
    @Test
    public void testNonNegativeQuality() {
        Item item = new Item("Custom unique", 0, 10);
        this.gildedRose.items.add(item);
        this.gildedRose.updateQuality();

        assertTrue("The quality never is negative", item.getQuality() >= 0);
    }

    /**
     *  El Quality disminuye su valor con  el tiempo.
     */
    @Test
    public void testReduceQualityWhenSellInIsNotZero() {
        int sellIn = 5;
        int quality = 10;
        int reduction = 1;

        Item item = new Item("Normal Item", sellIn, quality);
        this.gildedRose.items.add(item);

        this.gildedRose.updateQuality();
        assertEquals(quality - reduction, item.getQuality());
    }

    /**
     *  El Quality disminuye el doble de su valor cuando sellIn es 0
     */
    @Test
    public void testReduceQualityWhenSellInIsZero() {
        int sellIn = 0;
        int quality = 10;
        int reduction = 2;

        Item item = new Item("Normal Item", sellIn, quality);
        this.gildedRose.items.add(item);

        this.gildedRose.updateQuality();
        assertEquals(quality - reduction, item.getQuality());
    }

    @Test
    public void testIncrementQualityForAgedBrieItem() {
        int quality = 3;
        Item agedBrie = new Item("Aged Brie", 2, quality);
        this.gildedRose.items.add(agedBrie);

        this.gildedRose.updateQuality();

        assertEquals(4, agedBrie.getQuality());
    }

    @Test
    public void testTheQualityIsNeverMoreThanFifty() {
        int quality = 50;
        Item agedBrie = new Item("Aged Brie", 2, quality);
        this.gildedRose.items.add(agedBrie);

        this.gildedRose.updateQuality();
        this.gildedRose.updateQuality();
        this.gildedRose.updateQuality();

        assertEquals(50, agedBrie.getQuality());
    }

    @Test
    public void testTheSulfurasQualityNeverChange() {
        int quality = 80;
        Item sulfuras = new Item("Sulfuras, Hand of Ragnaros", 5, quality);
        this.gildedRose.items.add(sulfuras);

        this.gildedRose.updateQuality();
        this.gildedRose.updateQuality();
        this.gildedRose.updateQuality();

        assertEquals(quality, sulfuras.getQuality());
    }
    @Test
    public void testBackStagePassesCaseQualityIncreaseOne() {
        int quality = 20;
        int increment=1;
        Item sulfuras = new Item("Backstage passes to a TAFKAL80ETC concert", 15, quality);
        this.gildedRose.items.add(sulfuras);

        this.gildedRose.updateQuality();

        assertEquals(quality + increment, sulfuras.getQuality());
    }
    @Test
    public void testBackStagePassesCaseQualityIncreaseTwo() {
        int quality = 20;
        int increment=2;
        Item sulfuras = new Item("Backstage passes to a TAFKAL80ETC concert", 8, quality);
        this.gildedRose.items.add(sulfuras);

        this.gildedRose.updateQuality();

        assertEquals(quality + increment, sulfuras.getQuality());
    }
    @Test
    public void testBackStagePassesCaseQualityIncreaseThree() {
        int quality = 20;
        int increment=3;
        Item sulfuras = new Item("Backstage passes to a TAFKAL80ETC concert", 3, quality);
        this.gildedRose.items.add(sulfuras);

        this.gildedRose.updateQuality();

        assertEquals(quality + increment, sulfuras.getQuality());
    }
    @Test
    public void testBackStagePassesCaseQualityDropped() {
        int quality = 20;

        Item sulfuras = new Item("Backstage passes to a TAFKAL80ETC concert", 0, quality);
        this.gildedRose.items.add(sulfuras);

        this.gildedRose.updateQuality();

        assertEquals(0, sulfuras.getQuality());
    }

    @Test
    public void testConjuredIncreaseQuality() {
        int quality = 6;
        int increase = 2;
        Item conjured = new Item("Conjured Mana Cake", 3, quality);
        this.gildedRose.items.add(conjured);

        this.gildedRose.updateQuality();

        assertEquals(quality + increase, conjured.getQuality());
    }

    @Test
    public void testNonNegativeQualityWhenSellInIsZero() {
        int quality = 1;
        Item any = new Item("Any item", 0, quality);
        this.gildedRose.items.add(any);

        this.gildedRose.updateQuality();

        assertEquals(0, any.getQuality());
    }
}
