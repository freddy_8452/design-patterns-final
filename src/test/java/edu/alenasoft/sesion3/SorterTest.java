package edu.alenasoft.sesion3;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Luis Roberto Perez on 12/10/2016.
 */
public class SorterTest {

    @Test
    public void testSortCountriesNames() {
        Sorter sorter = new Sorter();

        List countries = new ArrayList<String>();
        countries.add("Ecuador");
        countries.add("Brasil");
        countries.add("Bolivia");

        sorter.sortByName(countries);

        assertThat(countries.get(0).toString(), CoreMatchers.containsString("Bolivia"));
        assertThat(countries.get(1).toString(), CoreMatchers.containsString("Brasil"));
        assertThat(countries.get(2).toString(), CoreMatchers.containsString("Ecuador"));
    }

    @Test
    public void testSortCountriesByName() {
        Sorter sorter = new Sorter();

        List countries = new ArrayList<Country>();
        countries.add(new Country("Ecuador", 1500));
        countries.add(new Country("Brasil", 8000));
        countries.add(new Country("Bolivia", 1450));

        sorter.sortByName(countries);

        assertThat(countries.get(0).toString(), CoreMatchers.containsString("Bolivia"));
        assertThat(countries.get(1).toString(), CoreMatchers.containsString("Brasil"));
        assertThat(countries.get(2).toString(), CoreMatchers.containsString("Ecuador"));
    }

    @Test
    public void testSortCountriesByPeople() {
        Sorter sorter = new Sorter();

        List countries = new ArrayList<Country>();
        countries.add(new Country("Ecuador", 7500));
        countries.add(new Country("Brasil", 9800));
        countries.add(new Country("Bolivia", 1450));

        sorter.sortByPeople(countries);

        assertThat(countries.get(0).toString(), CoreMatchers.containsString("Bolivia"));
        assertThat(countries.get(1).toString(), CoreMatchers.containsString("Ecuador"));
        assertThat(countries.get(2).toString(), CoreMatchers.containsString("Brasil"));
    }
}