# POO Intermedio y Patrones de Diseño #

[Diapositivas del curso](https://docs.google.com/presentation/d/1DLqfuRagqZGrVyq8cOgQrdEH1nK8WEn7oCevbW4wFFo/edit?usp=sharing)

El código fuente del proyecto esta basado en un proyecto Java [Maven](https://maven.apache.org/).

El archivo diagrams.xml contiene todos los diagramas utilizados en el curso, este archivo puede ser abierto con [draw.io](https://www.draw.io/)

## Organización del Código: ##
El código se organiza en varios paquetes denominados "sesionX", cada uno de ellos contiene el código referido a las sesiones del curso, también una sesión puede contener el desarrollo de un software en particular.

## Software a instalar ##

 1. [Java Development Kit 8 (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
 2. [IntelliJ IDEA](https://www.jetbrains.com/idea/). Community Version

## Instrucciones para abrir el proyecto ##

1. Ejecutar IntelliJ
2. En la pantalla inicial seleccionar la opción ***Check out from Version Control*** -> ***Git***
3. En URL pegar la dirección *https://bitbucket.org/alenasoft/design-patterns.git*
4. Establecer un folder donde se guardará el proyecto
5. Asignar un nombre el proyecto
6. Presionar el botón **Clone** y aceptar la creación del proyecto.
7. Aceptar la opción de abrir el archivo **pom.xml**
  Sugerencia:
 - Habilitar auto importación (Enable Auto-Import)

## Bibliografía ##
- Horstmann, C. (2005). *OO Design & Patterns, 2nd ed*. Wiley Publisher.
- Gamma, E.; Vlissides, J.; Johnson, R. & Helm, R. (1994). *Design Patterns: Elements of Reusable Object-Oriented Software*. Addison-Wesley.